<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BaseAccount;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('version', function(){
    // to do - from env
    return response()->json([
        "version" => "x"
    ]);
})->name('api.version');

// ->middleware(['auth'])
Route::prefix('account')->group(function(){
    Route::post('create', [BaseAccount::class, 'create'])->name('admin.account.create');
    // 'api/account/get'
    // 'api/account/update'
    // 'api/account/reset'
});