<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\TicketController;
use App\Http\Controllers\Admin\TicketLogController;
use App\Http\Controllers\Admin\HomeController as AdminHomeCtrl;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\ContactsController;
use App\Http\Controllers\Admin\MeetingsController;
use App\Http\Controllers\Admin\ProjectsController;

use App\Http\Controllers\Client\HomeController as ClientHomeCtrl;
use App\Http\Controllers\Client\TicketController as ClientTicketCtrl;

use App\Http\Controllers\Admin\Datatables\UserController as DtUser;
use App\Http\Controllers\Admin\Datatables\CompanyController as DtCompany;
use App\Http\Controllers\Admin\Datatables\ContactsController as DtContacts;
use App\Http\Controllers\Admin\Datatables\MeetingsController as DtMeetings;
use App\Http\Controllers\Admin\Datatables\ProjectController as DtProjects;
use App\Http\Controllers\Admin\Datatables\TagsController as DtTags;
use App\Http\Controllers\Admin\Datatables\TeamsController as DtTeams;
use App\Http\Controllers\Admin\Datatables\TicketsController as DtTickets;
use App\Http\Controllers\Admin\Datatables\TicketsLogsController as DtTicketsLogs;

use App\Http\Controllers\Admin\Datatables\ClientTicketsController as DtClientTickets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * todo:
 * middlewares
 * - admin
 * - isactive
 * 
 */

Route::prefix('')->group(function(){
    // group
});


Route::post("trello", function(Request $r){
    // group
	Log::info(file_get_contents("php://input"));
	Log::info(presonRet($r->all()));
	return 200;
});

allRoutes();

Route::prefix('shared')
->middleware(['auth'])
->group(function(){
	datatableRoute("users", DtUser::class);
	datatableRoute("companies", DtCompany::class);
	datatableRoute("contacts", DtContacts::class);
	datatableRoute("meetings", DtMeetings::class);
	datatableRoute("projects", DtProjects::class);
	datatableRoute("tags", DtTags::class);
	datatableRoute("teams", DtTeams::class);
	datatableRoute("tickets", DtTickets::class);
	datatableRoute("tickets-logs", DtTicketsLogs::class);
	Route::get('/data/tickets-logs/filter', [DtTicketsLogs::class, 'filter'])
		->name('admin.tickets-logs.datatable.filter');

	datatableRoute("client.tickets", DtClientTickets::class);
	Route::get('/data/client/tickets/filter', [DtClientTickets::class, 'filter'])->name('admin.client.tickets.datatable.filter');
	
	Route::get('/data/tickets/withagent', [DtTickets::class, 'withAgent'])->name('admin.tickets.datatable.withAgent');
	Route::get('/data/tickets/filter', [DtTickets::class, 'filter'])->name('admin.tickets.datatable.filter');
});

Route::prefix('superadmin')
->middleware(['auth'])
->group(function(){
	Route::get('/', [AdminHomeCtrl::class, 'index'])->name('admin.home');
    // group
    Route::get('tickets/status/{status}', function () {
	    return "tickets/status/{status}";
	})->name("superadmin.tickets.status.{status}");

	resourceCrudWithPrefix("companies", "superadmin.companies", CompanyController::class);
	resourceCrudWithPrefix("contacts", "superadmin.contacts", ContactsController::class);
	resourceCrudWithPrefix("meetings", "superadmin.meetings", MeetingsController::class);
	resourceCrudWithPrefix("projects", "superadmin.projects", ProjectsController::class);
	Route::get("/projects/bycompany/{id}", [ProjectsController::class, "byCompany"])
		->name("superadmin.projects.bycompany");
	resourceCrudWithPrefix("teams", "superadmin.teams", Teams::class);
	resourceCrudWithPrefix("tickets", "superadmin.tickets", TicketController::class);
	resourceCrudWithPrefix("ticket-log", "superadmin.tickets-logs", TicketLogController::class);
	Route::post("/tickets-log/t/{id}", [TicketLogController::class, "createFromTicket"])
		->name("superadmin.tickets-logs.createFromTicket");
	Route::get("/tickets-log/t/{id}", [TicketLogController::class, "byTicket"])
		->name("superadmin.tickets-logs.byticket");
	Route::get("/tickets/complete/{id}", [TicketController::class, "getComplete"])
		->name("superadmin.tickets.complete");
		
	Route::get("/tickets", [TicketController::class, "indexTbl"])->name("superadmin.tickets.index");
	Route::get("/tickets-table", [TicketController::class, "indexTbl"])->name("superadmin.ticket.index-tbl");
	resourceCrudWithPrefix("users", "superadmin.users", UserController::class);

	Route::get('tickets/status/{status}', function () {
	    return "tickets/status/{status}";
	})->name("superadmin.tickets.status.{status}");
		
	Route::get('settings', function () {
	    return "settings";
	})->name("superadmin.settings");
	
});

Route::prefix('client')->group(function(){
	Route::get('/', [ClientHomeCtrl::class, 'index'])->name('client.home');
	resourceCrudWithPrefix("tickets", "client.tickets", ClientTicketCtrl::class);
	Route::get("/tickets/complete/{id}", [ClientTicketCtrl::class, "getComplete"])
		->name("client.tickets.complete");
	
	Route::get('ticket/m/{uuid}', function () {
	    return "ticket/m/{uuid}";
	})->name("u.ticket.m.{uuid}");
	
});

Route::prefix('support')->group(function(){
    // group
    
	Route::get('tickets/cru/', function () {
	    return "tickets/cru/";
	})->name("support.tickets.cru");
	
	Route::get('tickets/status/', function () {
	    return "tickets/status/";
	})->name("support.tickets.status");
	
});

Route::prefix('manager')->group(function(){
    // group
    	
	resourceCrudWithPrefix("companies", "manager.companies", Companies::class);	
	resourceCrudWithPrefix("contacts", "manager.contacts", Contacts::class);	
	resourceCrudWithPrefix("projects", "manager.projects", Projects::class);
});



Auth::routes();

// Route::get('/auth/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('auth.login');
Route::get('/auth/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('auth.logout');
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
