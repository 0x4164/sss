<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TicketStatus;

// php artisan db:seed --class=TicketsStatusSeeder
class TicketsStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $arr = ['pending', 'open', 'in-progress', 'hold', 'solved', 'close'];
            foreach($arr as $e){
                $u = TicketStatus::create([
                    "name" => $e,
                    "descr" => $e,
                    "short" => $e,
                ]);
            }
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
