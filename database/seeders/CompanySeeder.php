<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Tag;

// php artisan db:seed --class=CompanySeeder
class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $t = Tag::where("type", "lembaga")->first();
            Company::create([
                "name" => "company",
                "descr" => "descr",
                "address" => "Jl Jalan",
                "lembaga" => $t->id,
                "main_contact" => "089333444111",
            ]);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
