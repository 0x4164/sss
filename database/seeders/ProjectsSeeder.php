<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use App\Models\Projects;

// php artisan db:seed --class=ProjectsSeeder
class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $c = Company::first();

            $cp = $c->project();
            $cp->create([
                "name" => "Nama Projek 1",
                "descr" => "descr",
                "tahun" => 2021,
            ]);

            $cp->create([
                "name" => "Nama Projek 2",
                "descr" => "descr",
                "tahun" => 2021,
            ]);

            $cp->create([
                "name" => "Nama Projek 3",
                "descr" => "descr",
                "tahun" => 2021,
            ]);
            
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
