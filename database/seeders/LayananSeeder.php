<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\Layanan;
use App\Models\Instansi;

// php artisan db:seed --class=LayananSeeder
class LayananSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Layanan::factory()
        //     ->count(3)
        //     ->create();
        
        $ins = Instansi::all();

        foreach($ins as $key => $in){
            $ly = $in->layanan()->create([
                "nama" => "layanan".$key,
                "deskripsi" => "layanan".$key,
                "kode" => $key,
            ]);
            foreach(range(1,5) as $n){
                $now = $n <= 3 ? Carbon::now() : null;
                $ly->antrian()->create([
                    "no_antrian" => $n,
                    "called_at" => $now,
                ]);
            }
        }
    }
}
