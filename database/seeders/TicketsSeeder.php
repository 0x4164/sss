<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tickets;
use App\Models\User;
use App\Models\Company;
use App\Models\Projects;
use App\Models\Tag;

// php artisan db:seed --class=TicketsSeeder
class TicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $c = Company::first();
            $p = Projects::first();
            $u = User::first();
            $t = Tag::first();

            $u->tickets()->create([
                "name" => "Ticket",
                "descr" => "Saya punya masalah di bug",
                "by" => $u->id,
                "agent" => $u->id,
                "last_status" => "pending",
                "company_id" => $c->id,
                "project_id" => $p->id,
                "category" => $t->id,
                "tag" => $t->id,
            ]);

        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
