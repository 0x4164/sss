<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Meetings;

// php artisan db:seed --class=MeetingsSeeder
class MeetingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            Meetings::create([
                "name" => "Meeting 1",
                "descr" => "descr",
                "by" => "CEO",
                "held_at" => now(),
                "result" => "Meeting results",
                "location" => "Zoom",
            ]);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
