<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Teams;

// php artisan db:seed --class=TeamsSeeder
class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            Teams::create([
                "name" => "Nama Projek",
                "descr" => "descr",
                "main_contact" => "083222999888",
            ]);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
