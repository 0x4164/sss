<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;
use Illuminate\Support\Str;

// php artisan db:seed --class=TagsSeeder
class TagsSeeder extends Seeder
{
    public function getFmt(
        $name,
        $type="",
        $slug=""
    ){
        $slug = Str::slug($name);

        return [
            "name" => $name,
            "slug" => $slug,
            "type" => $type,
        ];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $ds = [
            $this->getFmt(
                "Bug issue",
                "tiket"
            ),
            $this->getFmt(
                "Feature request",
                "tiket"
            ),
            $this->getFmt(
                "Administration Issue",
                "tiket"
            ),
            $this->getFmt(
                "General Issue",
                "tiket"
            ),
            $this->getFmt(
                "Kejaksaan",
                "lembaga"
            ),
            $this->getFmt(
                "Pengadilan",
                "lembaga"
            ),
            $this->getFmt(
                "Swasta",
                "lembaga"
            ),
        ];
        
        foreach($ds as $d){
            break;
            try{
                Tag::create($d);
            }catch(\Exception $e){
                echo $e->getMessage();
            }
        }

        $arr = ['pending', 'open', 'in-progress', 'hold', 'solved', 'close'];
        
        foreach($arr as $e){
            try{
                Tag::create(
                    $this->getFmt(
                        ucfirst($e),
                        "ticket-status"
                    )
                );
            }catch(\Exception $e){
                echo $e->getMessage();
            }
        }
    }
}
