<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tickets;
use App\Models\User;
use App\Models\Company;
use App\Models\Projects;
use App\Models\Tag;

// php artisan db:seed --class=TicketsLogSeeder
class TicketsLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            $u = User::first();
            $p = Tickets::first();

            $p->logs()->create([
                "descr" => "Log: Saya punya masalah di bug",
                "agent" => $u->id,
                "status" => "pending",
            ]);

        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
