<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contacts;

// php artisan db:seed --class=ContactsSeeder
class ContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            Contacts::create([
                "name" => "kontak",
                "descr" => "descr",
                "telp" => "089333444111",
                "address" => "Jl Jalan",
            ]);
        }catch(\Exception $e){
            echo $e->getMessage();
        }
    }
}
