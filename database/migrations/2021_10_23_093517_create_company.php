<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompany extends StdMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('company', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table) use ($ctx) {
                $table->string("address")->nullable();
                $table->string("main_contact")->nullable();
                $ctx->unsignedForeign($table, "tags", "lembaga");
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
