<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTickets extends StdMigration
{
    protected $useDescr = false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('tickets', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table) use ($ctx) {
                $ctx->unsignedForeign($table, "tags", "category");
                $ctx->unsignedForeign($table, "users", "by");
                $ctx->unsignedForeign($table, "users", "agent");
                $ctx->unsignedForeign($table, "company", "company_id");
                $ctx->unsignedForeign($table, "projects", "project_id");
                $table->text('descr')->comment("description");
                $table->string("last_status");
                $ctx->unsignedForeign($table, "tags", "tag");
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
