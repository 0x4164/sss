<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsLogs extends StdMigration
{
    protected $useName = false;
    protected $useDescr = true;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('tickets_logs', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table) use ($ctx) {
                $ctx->unsignedForeign($table, "tickets", "tickets_id");
                $ctx->unsignedForeign($table, "users", "agent");
                $table->string("status")->comment('status ticket at the time');
            });
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_logs');
    }
}
