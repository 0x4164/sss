<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeams extends StdMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('teams', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table){
                $table->string('main_contact');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
