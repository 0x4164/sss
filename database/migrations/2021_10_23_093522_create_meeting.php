<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeeting extends StdMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('meetings', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table){
                $table->string("by");
                $table->dateTime("held_at");
                $table->text("result");
                $table->string("location");
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
