<?php

use App\Migrations\StdMigration;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjects extends StdMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ctx = $this;
        Schema::create('projects', function (Blueprint $table) use ($ctx) {
            $ctx->standard($table, function($table) use ($ctx) {
                $table->integer("tahun");
                $ctx->unsignedForeign($table, "company", "company_id");
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
