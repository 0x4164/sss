<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware("roles:superadmin|admin");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try{
            $user = auth()->user();
            
            return view('admin.home');
        }catch (\Exception $e) {
            return self::fail($e->getMessage(), null);
        }
    }

    public function refresh(){
        try{
            $user = auth()->user();
            $user->instansi;
    
            $withAntrian = $this->layanan->retrieveWithGenerate(
                $user->instansi->id
            );
            
            return self::success("Ok", $withAntrian);
        }catch (\Exception $e) {
            return self::fail($e->getMessage(), null);
        }
    }

    public function generate(){
        $ret = $this->layanan->generateAntrian(['id' => 7]);

        return response()->json($ret);
    }
}
