<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Repositories\ProjectsRepository;
use Illuminate\Http\Request;
use App\Models\Projects;

class ProjectsController extends JsonCrudController
{
    protected $blades = [
        "index" => "admin.projects.index",
        "add" => "admin.projects.add",
        "edit" => "admin.projects.edit",
    ];
    
    public function __construct(
        ProjectsRepository $dataRepository
    ) {
        $this->dataRepository = $dataRepository;
        $this->middleware("roles:superadmin|admin|manager");
    }

    public function index(Request $request){
        try {
            $data = [];
            $company = Company::all();

            return view($this->blades['index'], compact('data', 'company'));
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function store(Request $request){
        $data = null;
        try {
            $attributes = $request->all();
            $user = auth()->user();
            $data = $this->dataRepository->store($attributes);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
    
    public function byCompany($id){
        $data = null;
        try {
            $data = (new Projects)->byCompany($id)->get();

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
