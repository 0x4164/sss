<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ProjectsRepository;
use Illuminate\Http\Request;

class MeetingsController extends JsonCrudController
{
    protected $blades = [
        "index" => "admin.meetings.index",
        "add" => "admin.meetings.add",
        "edit" => "admin.meetings.edit",
    ];
    
    public function __construct(
        ProjectsRepository $dataRepository
    ) {
        $this->dataRepository = $dataRepository;
        $this->middleware("roles:superadmin|admin|manager");
    }

    public function index(Request $request){
        try {
            $data = [];

            return view($this->blades['index'], compact('data'));
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function store(Request $request)
    {
        $data = null;
        try {
            $attributes = $request->all();
            $user = auth()->user();
            $data = $this->dataRepository->store($attributes);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
