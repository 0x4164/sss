<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tickets;
use App\Repositories\TicketLogRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Models\Tag;

class TicketLogController extends JsonCrudController
{
    protected $blades = [
        "index" => "admin.tiket.index-log",
        "add" => "admin.tiket.add",
        "edit" => "admin.tiket.edit",
    ];
    
    public function __construct(
        TicketLogRepository $dataRepository,
        UserRepository $userRepo
    ) {
        $this->dataRepository = $dataRepository;
        $this->userRepo = $userRepo;
        $this->middleware("roles:superadmin|admin|manager");
    }

    public function index(Request $request){
        try {
            $agents = $this->userRepo->getAllAgents();

            return view($this->blades['index'], [compact('agents')]);
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function byTicket(Request $request, $id){
        try {
            $agents = $this->userRepo->getAllAgents();
            $ticket = Tickets::where("id", $id)->firstOrFail();
            $ticketStats = (new Tag)->ticketStatus()->get();
            
            return view($this->blades['index'], compact('agents', 'ticket', 'ticketStats'));
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function createFromTicket(Request $request, $id) {
        $data = null;
        try {
            $attr = $request->all();
            $data = $this->dataRepository->createFromTicket($id, $attr);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
