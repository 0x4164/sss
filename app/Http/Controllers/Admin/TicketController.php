<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\TicketRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Helpers\APIHelper;
use App\Models\Company;
use App\Models\Projects;
use App\Models\Tickets;
use App\Models\Tag;

class TicketController extends JsonCrudController
{
    protected $blades = [
        "index" => "admin.tiket.index",
        "add" => "admin.tiket.add",
        "edit" => "admin.tiket.edit",
    ];
    
    public function __construct(
        TicketRepository $dataRepository,
        UserRepository $userRepo
    ) {
        $this->dataRepository = $dataRepository;
        $this->userRepo = $userRepo;
        $this->middleware("roles:superadmin|admin|manager");
    }

    public function index(Request $request){
        try {
            $agents = $this->userRepo->getAllAgents();

            return view($this->blades['index'], compact('agents'));
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    protected function getIndexData(){
        $agents = $this->userRepo->getAllAgents();
        $comp = Company::all();
        $proj = Projects::all();
        $ticketStats = Tickets::STATUS;
        $tags = (new Tag)->ticketType()->get();

        return compact('agents', 'ticketStats', 'comp', 'proj', 'tags');
    }

    public function indexTbl(Request $request){
        try {
            return view("admin.tiket.index-tbl", 
                $this->getIndexData()
            );
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function getComplete($id) {
        $data = null;
        try {
            $data = $this->dataRepository->getComplete($id);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
    
    public function store(Request $request) {
        $data = null;
        try {
            $attr = $request->all();
            $user = auth()->user();
            $data = $this->dataRepository->createByUser($user, $attr);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
