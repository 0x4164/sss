<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\LayananRepository;
use Illuminate\Http\Request;
use App\Helpers\APIHelper;

class LayananController extends JsonCrudController
{
    protected $blades = [
        "index" => "admin.layanan.index",
        "add" => "admin.layanan.add",
        "edit" => "admin.layanan.edit",
    ];
    
    public function __construct(
        LayananRepository $dataRepository
    ) {
            $this->dataRepository = $dataRepository;
    }

    public function index(Request $request){
        try {
            $user = auth()->user();
            $data = $user->instansi->layanan;

            return view($this->blades['index'], compact('data'));
        } catch (\Exception $th) {
            return $th->getMessage();
            // return redirect()->route('admin.data.index')->with('error', $th->getMessage());
        }
    }

    public function store(Request $request)
    {
        $data = null;
        try {
            $attributes = $request->all();
            $user = auth()->user();
            $data = $this->dataRepository->create($user, $attributes);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
