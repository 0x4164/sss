<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Layanan;
use DB;

class LayananController extends DTControllerBase
{
    protected $model = Layanan::class;

    public function get(){
        $user = auth()->user();
        $data = $user->instansi->layanan;
        
        return datatables()->of($data)->toJson();
    }
}
