<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Contacts;

class ContactsController extends DTControllerBase
{
    protected $model = Contacts::class;
}
