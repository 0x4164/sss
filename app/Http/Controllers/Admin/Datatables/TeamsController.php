<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Teams;

class TeamsController extends DTControllerBase
{
    protected $model = Teams::class;
}
