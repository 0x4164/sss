<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Repositories\TicketRepository;
use App\Models\Tickets;
use Arr;

class ClientTicketsController extends TicketsController
{
    protected $model = Tickets::class;

    public function get(){
        return $this->filter();
    }

    public function filter(TicketRepository $ticketRepo){
        $this->setTicketWheres();
        $user = auth()->user();
        $this->addWheres("by", $user->id);
        $wq = $this->getWheres();

        return datatables()->of(
            $ticketRepo->baseComplete()->where($wq)->orderBy('id',"desc")
        )->toJson();
    }

    public function withAgent(){
        return datatables()->of(
            $this->model::with(['agent'])->orderBy('id',"desc")
        )->toJson();
    }
}
