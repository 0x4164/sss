<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Meetings;
use DB;

class MeetingsController extends DTControllerBase
{
    protected $model = Meetings::class;
}
