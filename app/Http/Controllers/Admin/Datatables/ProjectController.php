<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Projects;
use DB;

class ProjectController extends DTControllerBase
{
    protected $model = Projects::class;
}
