<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Tag;

class TagsController extends DTControllerBase
{
    protected $model = Tag::class;
}
