<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Tickets;
use App\Repositories\TicketRepository;
use Arr;

class TicketsController extends DTControllerBase
{
    protected $model = Tickets::class;

    public function setTicketWheres(){
        $wheres = Arr::get($_GET, "wheres");
        $wq = [];
        if($wheres){
            $status = Arr::get($wheres, "last_status");
            if($status && $status !== "-"){
                $this->addWheres("last_status", $status);
            }

            $project = Arr::get($wheres, "project_id");
            if(self::isValidParam($project)){
                $this->addWheres("project_id", $project);
            }
        }

        return $this;
    }

    public function filter(TicketRepository $ticketRepo){
        $this->setTicketWheres();
        $wq = $this->getWheres();

        return datatables()->of(
            $ticketRepo->baseComplete()->where($wq)->orderBy('id',"desc")
        )->toJson();
    }

    public function withAgent(){
        return datatables()->of(
            $this->model::with(['agent'])->orderBy('id',"desc")
        )->toJson();
    }
}
