<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Company;

class CompanyController extends DTControllerBase
{
    protected $model = Company::class;
}
