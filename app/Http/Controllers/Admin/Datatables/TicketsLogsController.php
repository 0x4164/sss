<?php

namespace App\Http\Controllers\Admin\Datatables;

use App\Http\Controllers\Admin\Datatables\DTControllerBase;
use App\Models\Tickets;
use App\Models\TicketsLogs;
use Arr;

class TicketsLogsController extends DTControllerBase
{
    protected $model = TicketsLogs::class;

    public function setTicketWheres(){
        $wheres = Arr::get($_GET, "wheres");
        $wq = [];
        if($wheres){
            $tickets_id = Arr::get($wheres, "tickets_id");
            if($tickets_id && $tickets_id !== "-"){
                $this->addWheres("tickets_id", $tickets_id);
            }
        }

        return $this;
    }

    public function filter(){
        $this->setTicketWheres();
        $wq = $this->getWheres();

        return datatables()->of(
            $this->model::with(['agent'])->where($wq)->orderBy('id',"desc")
        )->toJson();
    }

    public function withAgent(){
        return datatables()->of(
            $this->model::with(['agent'])->orderBy('id',"desc")
        )->toJson();
    }
}
