<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Admin\TicketController as AdminTicketCtrl;
use App\Repositories\ClientTicketRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Projects;
use App\Models\Tickets;

class TicketController extends AdminTicketCtrl
{
    protected $blades = [
        "index" => "client.tiket.index",
        "add" => "client.tiket.add",
        "edit" => "client.tiket.edit",
    ];

    public function __construct(
        ClientTicketRepository $dataRepository,
        UserRepository $userRepo
    ) {
        $this->dataRepository = $dataRepository;
        $this->userRepo = $userRepo;
        $this->middleware("roles:client|support|manager|superadmin");
    }

    public function index(Request $request){
        try {
            return view("admin.tiket.index-tbl", 
                $this->getIndexData()
            );
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function store(Request $request) {
        $data = null;
        try {
            $attr = $request->all();
            $data = $this->dataRepository->createByCurrentUser($attr);

            return self::success("Ok", $data);
        } catch (\Exception $th) {
            return self::fail($th->getMessage(), $data);
        }
    }
}
