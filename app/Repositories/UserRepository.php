<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;
use App\Models\Tickets;
use App\Models\User;
use DB;
use Arr;

class UserRepository extends BaseRepository{
    use CRUDRepoTrait;
    protected $mainTable = "users";

    public function __construct(){
        $this->setModel(User::class);
    }
    
    public function insertData($attributes){
        $this->logsEvent($attributes);

        return [
            'name' => $attributes['name'], 
            'username' => $attributes['username'], 
            'email' => $attributes['email'], 
            'email_verified_at' => now(), 
            'role' => $attributes['role'], 
            'is_active' => $attributes['is_active'], 
            'password' => $attributes['password'],
        ];
    }

    public function updateData($attributes){
        
        $data = [
            'name' => $attributes['name'], 
            'username' => $attributes['username'], 
            'email' => $attributes['email'], 
            'role' => $attributes['role'], 
            'is_active' => $attributes['is_active'], 
        ];
        
        if(! empty(Arr::get($attributes, 'email_verified_at'))){
            $data['email_verified_at'] = now();
        }

        if(!empty($attributes['password']) && !empty($attributes['repassword'])){
            $data['password'] = bcrypt($attributes['password']);
        }
        $this->logsEvent($attributes);
        
        return $data;
    }

    public function allowDelete($data){
        $c = !in_array($data->username, User::BLACKLIST_DELETE);
        return $c;
    }

    public function getAllAgents(){
        $d = $this->getMainDB()->select('*')->whereIn("role", User::AGENT_BY_ROLES)->get();
        
        return $d;
    }
}