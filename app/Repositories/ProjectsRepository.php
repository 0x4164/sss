<?php
namespace App\Repositories;

use App\Models\Projects;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;

class ProjectsRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Projects::class);
    }
    
    public function insertData($attributes){
        $this->logsEvent($attributes);

        return [
            "name" => $attributes["name"], 
            "descr" => $attributes["descr"], 
            "tahun" => $attributes["tahun"], 
            "company_id" => $attributes["company_id"], 
        ];
    }

    public function updateData($attributes){
        return [
            "name" => $attributes["name"], 
            "descr" => $attributes["descr"], 
            "tahun" => $attributes["tahun"], 
            "company_id" => $attributes["company_id"], 
        ];
    }
}