<?php
namespace App\Repositories;

use App\Models\Tickets;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;
use App\Models\Tag;
use App\Models\TicketsLogs;
use App\Services\EventService;
use DB;

class TicketLogRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(EventService $evtSvc){
        $this->setModel(TicketsLogs::class);
        $this->setEventService($evtSvc);
    }
    
    public function insertData($attributes){
        return [
            "descr" => $attributes['descr'],
            "status" => $attributes['status'],
            "agent" => (int) $attributes['agent'],
        ];
    }

    public function updateData($attributes){
        return [
            "descr" => $attributes['descr'],
            "status" => $attributes['status'],
            "agent" => (int) $attributes['agent'],
        ];
    }

    public function doUpdate($dbData, $data){
        $dbData->update($data);
        $dbData->agent = $data['agent'];
        $dbData->save();

        return $dbData;
    }

    public function createFromTicket($ticketId, $attr){
        $data = $this->insertData($attr);
        $ticket = Tickets::where("id", $ticketId)->first();
        
        $ticketStats = (new Tag)->ticketStatus()->get();
        $tag = $ticketStats->where("id", $attr['status'])->first();
        $ticket->last_status = $tag->slug;
        $ticket->agent = $attr['agent'];
        $ticket->save();

        $created = $ticket->logs()->create($data);
        $this->onUpdated($ticket);

        return $created;
    }

    public function onUpdated($data = null){
        return $this->eventService->trigger("ticket-updated", $data);
    }
}