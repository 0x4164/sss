<?php
namespace App\Repositories;

use Carbon\Carbon;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;
use App\Models\Antrian;
use DB;

class AntrianRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Antrian::class);
    }
    
    public function insertData($attributes){
        return [
            'no_antrian' => $attributes['name'],
            'layanan_id' => $attributes['layanan_id'],
            'called_at' => $attributes['called_at'],
            'ended_at' => $attributes['ended_at'],
        ];
    }

    public function updateData($attributes){
        return [
            'no_antrian' => $attributes['name'],
            'layanan_id' => $attributes['email'],
            'called_at' => $attributes['called_at'],
            'ended_at' => $attributes['ended_at'],
        ];
    }

    public function call($id_layanan, $no_antrian){
        $data = [];
        $data = $this->model::where([
            "layanan_id" => $id_layanan,
            "no_antrian" => $no_antrian
        ])
        ->whereDate('created_at', '=', date('Y-m-d'))
        ->first();

        if($data){
            $data->called_at = now();
            $data->update([
                "called_at" => Carbon::now()
            ]);
        }else{
            throw new \Exception("Antrian / layanan belum ada");
        }

        return $data;
    }
}