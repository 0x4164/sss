<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;
use App\Models\Layanan;
use App\Models\Antrian;
use App\Models\Instansi;
use DB;

class LayananRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Layanan::class);
    }
    
    public function insertData($attributes){
        return [
            'nama' => $attributes['nama'],
            'deskripsi' => $attributes['deskripsi'],
            'kode' => $attributes['kode'],
            'instansi_id' => $attributes['instansi_id'],
        ];
    }

    public function updateData($attributes){
        return [
            'nama' => $attributes['nama'],
            'deskripsi' => $attributes['deskripsi'],
            'kode' => $attributes['kode'],
        ];
    }
}