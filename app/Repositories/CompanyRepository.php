<?php
namespace App\Repositories;

use App\Models\Company;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;

class CompanyRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Company::class);
    }
    
    public function insertData($attributes){
        $this->logsEvent($attributes);

        return [
            'name' => $attributes['name'], 
            'descr' => $attributes['descr'], 
            'address' => $attributes['address'], 
            'main_contact' => $attributes['main_contact'], 
            'lembaga' => $attributes['lembaga'], 
        ];
    }

    public function updateData($attributes){
        return [
            'name' => $attributes['name'], 
            'descr' => $attributes['descr'], 
            'address' => $attributes['address'], 
            'main_contact' => $attributes['main_contact'], 
            'lembaga' => $attributes['lembaga'], 
        ];
    }
}