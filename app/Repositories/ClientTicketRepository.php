<?php
namespace App\Repositories;

class ClientTicketRepository extends TicketRepository{
    public function doUpdate($dbData, $data){
        $dbData->update($data);
        $dbData->project_id = $data['project_id'];
        $dbData->company_id = $data['company_id'];
        $dbData->save();

        return $dbData;
    }

    public function createByCurrentUser($attr){
        return $this->createByUser(auth()->user(), $attr);
    }
}