<?php
namespace App\Repositories;

use App\Models\Teams;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;

class TeamsRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Teams::class);
    }
    
    public function insertData($attributes){
        $this->logsEvent($attributes);

        return [
            'name' => $attributes['name'], 
            'descr' => $attributes['descr'], 
            'main_contact' => $attributes['main_contact'], 
        ];
    }

    public function updateData($attributes){
        return [
            'name' => $attributes['name'], 
            'descr' => $attributes['descr'], 
            'main_contact' => $attributes['main_contact'], 
        ];;
    }
}