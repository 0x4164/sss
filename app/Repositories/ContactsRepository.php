<?php
namespace App\Repositories;

use App\Models\Contacts;
use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;

class ContactsRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(){
        $this->setModel(Contacts::class);
    }
    
    public function insertData($attributes){
        $this->logsEvent($attributes);

        return [
            "name" => $attributes["name"], 
            "descr" => $attributes["descr"], 
            "telp" => $attributes["telp"], 
            "address" => $attributes["address"], 
        ];
    }

    public function updateData($attributes){
        return [
            "name" => $attributes["name"], 
            "descr" => $attributes["descr"], 
            "telp" => $attributes["telp"], 
            "address" => $attributes["address"], 
        ];
    }
}