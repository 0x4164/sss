<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Repositories\Traits\CRUDRepoTrait;
use App\Models\Tickets;
use App\Services\EventService;
use DB;

class TicketRepository extends BaseRepository{
    use CRUDRepoTrait;

    public function __construct(EventService $evtSvc){
        $this->setModel(Tickets::class);
        $this->setEventService($evtSvc);
    }
    
    public function insertData($attributes){
        return [
            "name" => $attributes['name'],
            "descr" => $attributes['descr'],
            "category" => $attributes['category'],
            "tag" => $attributes['category'],
            "by" => $attributes['by'],
            "last_status" => "pending",
            "project_id" => $attributes['project_id'],
            "company_id" => $attributes['company_id'],
        ];
    }

    public function updateData($attributes){
        return [
            "name" => $attributes['name'],
            "descr" => $attributes['descr'],
            // "by" => $attributes['by'],
            "category" => $attributes['category'],
            "tag" => $attributes['category'],
            // "last_status" => "pending",
            // "agent" => (int) $attributes['agent'],
            "project_id" => $attributes['project_id'],
            "company_id" => $attributes['company_id'],
        ];
    }

    public function doUpdate($dbData, $data){
        $dbData->update($data);
        // $dbData->agent = $data['agent'];
        $dbData->project_id = $data['project_id'];
        $dbData->company_id = $data['company_id'];
        $dbData->save();

        return $dbData;
    }

    public function createByUser($user, $attr){
        $attr['by'] = $user->id;
        $data = $this->insertData($attr);
        $created = $user->tickets()->create($data);
        
        $this->onCreated($created);

        return $created;
    }

    public function getWithAgent($id){
        return $this->model::where(['id' => $id])->with('agent')->first();
    }

    // to do : optimize
    public function baseComplete(){
        return $this->model::with(['company','project','getAgent','byUser']);
    }

    public function getComplete($id){
        return $this->baseComplete()->where(['id' => $id])
        ->first();
    }
    
    public function getMultiComplete($where){
        return $this->baseComplete()->where($where)->get();
    }

    public function onCreated($data = null){
        return $this->eventService->trigger("ticket-created", $data);
    }

    public function onUpdated($data = null){
        return $this->eventService->trigger("ticket-updated", $data);
    }

    public function onDeleted($data = null){
        return $this->eventService->trigger("ticket-deleted", $data);
    }
}