<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $table = 'company';
    protected $fillable = [
        "name",
        "descr",
        "address",
        "main_contact",
        "lembaga",
    ];

    public function project(){
        return $this->hasOne(Projects::class);
    }

    public function projects(){
        return $this->hasMany(Projects::class);
    }
}
