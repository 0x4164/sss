<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketsLogs extends Model
{
    use HasFactory;

    protected $table = 'tickets_logs';
    protected $fillable = [
        "tickets_id",
        "status",
        "descr",
        "agent",
    ];

    // to do : tags
    public function agent(){
        return $this->belongsTo(User::class, "agent", "id");
    }
}
