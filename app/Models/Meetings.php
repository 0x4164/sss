<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meetings extends Model
{
    use HasFactory;

    protected $table = 'meetings';
    protected $fillable = [
        "name",
        "descr",
        "by",
        "held_at",
        "result",
        "location",
    ];
}
