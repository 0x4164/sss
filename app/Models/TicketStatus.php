<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    protected $table = 'tickets_status';
    protected $fillable = [
        "name",
        "descr",
        "short",
    ];

    // to do : tags
}
