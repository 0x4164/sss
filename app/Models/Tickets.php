<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    use HasFactory;
    use Taggable;

    const STATUS = [
        'pending', 'open', 'in-progress', 'hold', 'solved', 'close'
    ];

    protected $table = 'tickets';
    protected $fillable = [
        "name",
        "descr",
        "category",
        "by",
        "agent",
        "last_status",
        "company_id",
        "project_id",
        "tag",
    ];

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function byUser(){
        return $this->belongsTo(User::class, "by", "id");
    }

    public function project(){
        return $this->belongsTo(Projects::class);
    }

    public function getAgent(){
        return $this->belongsTo(User::class, "agent", "id");
    }
    
    public function logs(){
        return $this->hasMany(TicketsLogs::class);
    }

    // to do : tags
}
