<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'email_verified_at',
        'role',
        'is_active',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const ROLES = [
        "superadmin", "support", "manager", "client"
    ];
    
    const AGENT_BY_ROLES = [
        "superadmin", "support", "manager"
    ];

    const BLACKLIST_DELETE = [
        "superadmin"
    ];

    public function profile(){
        return $this->hasOne(UserProfile::class);
    }

    public function withProfile(){
        $this->profile;
        
        return $this;
    }

    public function tickets(){
        return $this->hasMany(Tickets::class, "agent", "id");
    }

    public function clientTickets(){
        return $this->hasMany(Tickets::class, "by", "username");
    }

    public function unresolvedTickets(){
        $this->tickets;
        return $this;
    }
    
    function isSuperadmin(){
        return $this->role === "superadmin";
    }

    function isManager(){
        return $this->role === "manager";
    }
}
