<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    use HasFactory;

    protected $table = 'projects';
    protected $fillable = [
        "name",
        "descr",
        "tahun",
        "company_id",
    ];

    public function byCompany($id){
        return $this->where("company_id", $id);
    }
}
