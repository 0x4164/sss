<?php
Schema::create('instansi', function (Blueprint $table) {
    $table->id();
    
    $table->unsignedBigInteger('user_id');
    $table->foreign('user_id')->references('id')->on('users')
        ->onUpdate('cascade')
        ->onDelete('cascade');

    $table->string("kode")->comment('kode satker')->unique();
    $table->string("parent");
    $table->string("nama");
    $table->integer("kelas");
    $table->string("kode_laporan");
    $table->string("alias")->unique();
    $table->string("akses");
    $table->boolean("is_active")->default(0);

    
    // mv to users
    // $table->string("email");
    // $table->string("username");
    // $table->string("password");
    $table->timestamps();
});