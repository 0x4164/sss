<?php

namespace App\Helpers;

/**
 * 
 * Static rest helper
 * @adib-enc
 * 
 */
class APIHelper{
    static function stdJson($success = true, $msg = "", $data = []){
        return [
            "success" => $success,
            "messsage" => $msg,
            "data" => $data
        ];
    }

    static function jsonResp($data = [], $code = null){
        $code = $code ?? 200;
        return response()->json($data, $code);
    }

    static function success($msg = "", $data = []){
        return response()->json(self::stdJson(true, $msg, $data), 200);
    }

    static function fail($msg = "", $data = []){
        return response()->json(self::stdJson(false, $msg, $data), 500);
    }
}