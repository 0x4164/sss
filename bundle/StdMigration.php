<?php

namespace App\Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StdMigration extends Migration
{
    protected $useName = true;
    protected $useDescr = true;
    protected $useTimestamp = true;
    
    protected function standard($table, $func){
        $table->id();
        
        if($this->useName){
            $table->string('name');
        }

        if($this->useDescr){
            $table->string('descr')->comment("description");
        }
        $func($table);
        
        if($this->useTimestamp){
            $table->timestamps();
        }

        return $this;
    }

    // todo "is_active",

    protected function unsignedForeign($table, $tbname, $col, $col2 = "id", $comment=""){
        $table->unsignedBigInteger($col)->comment($comment ?? "");
        $table->foreign($col)->references($col2)->on($tbname)
            ->onUpdate('cascade')
            ->onDelete('cascade');
        
        return $this;
    }

    protected function stringForeign($table, $tbname, $col, $col2 = "id"){
        $table->string($col);
        $table->foreign($col)->references($col2)->on($tbname)
            ->onUpdate('cascade')
            ->onDelete('cascade');
        
        return $this;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean("is_active")->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
