@php
$baseAsset = asset('assets/admin');
$user = auth()->user();
$role = $user->role;
$usericon = $baseAsset.'/img/user-icon.png';

$isSuperadmin = $role === "superadmin";
$isManager = $role === "manager";
$isSupport = $role === "support";
$isClient = $role === "client";

@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name_long') }}</title>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- dependent scripts -->
    <link rel="icon" type="image/x-icon" href="{{ $baseAsset }}/assets/img/favicon.png" />
    <link href="{{ $baseAsset }}/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script>
        const cl = (d) => {
            console.log(d)
        }
    </script>
</head>

<body>
    <nav class="topnav navbar navbar-expand shadow navbar-light bg-white" id="sidenavAccordion">
        <img class="img-fluid ml-3" src="{{ $baseAsset }}/img/logo.png" width="50px;" />
        <a class="navbar-brand" href="#">Ticketing Support <br><small>Sysindo</small></a>
        <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 mr-lg-2" id="sidebarToggle" href="#"><i data-feather="menu"></i></button>
       
        <ul class="navbar-nav align-items-center ml-auto">
            <li class="nav-item dropdown no-caret mr-3 d-none d-md-inline"> 
                Welcome back, <b> {{ $user->email }} </b> 
            </li>
            <li class="nav-item dropdown no-caret mr-2 dropdown-user">
                <a class="btn btn-icon btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage"
                    href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false"><img class="img-fluid" src="{{ $usericon }}" /></a>
                <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up"
                    aria-labelledby="navbarDropdownUserImage">
                    <h6 class="dropdown-header d-flex align-items-center">
                        <img class="dropdown-user-img" src="{{ $usericon }}" />
                        <div class="dropdown-user-details">
                            <div class="dropdown-user-details-name">{{ $user->name }}</div>
                            <div class="dropdown-user-details-email">{{ $user->email }}
                            </div>
                        </div>
                    </h6>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">
                        <div class="dropdown-item-icon"><i data-feather="settings"></i></div>
                        Account
                    </a>
                    <a class="dropdown-item" href="{{ route('auth.logout') }}">
                        <div class="dropdown-item-icon"><i data-feather="log-out"></i></div>
                        Logout
                    </a>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sidenav shadow-right sidenav-light">
                <div class="sidenav-menu">
                    <div class="nav accordion" id="accordionSidenav">
                        <div class="sidenav-menu-heading">Dashboard</div>
                        <a class="nav-link" href="{{ route('admin.home') }}">
                            <div class="nav-link-icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                    stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart">
                                    <line x1="12" y1="20" x2="12" y2="10"></line>
                                    <line x1="18" y1="20" x2="18" y2="4"></line>
                                    <line x1="6" y1="20" x2="6" y2="16"></line>
                                </svg></div>
                            Dashboard
                        </a>

                        <div class="sidenav-menu-heading">Menu</div>
                        <a class="nav-link" href="{{ route('client.tickets.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            My Tickets 
                        </a>
                        @if($isSuperadmin || $isManager)
                        <a class="nav-link" href="{{ route('superadmin.tickets.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Tickets
                        </a>
                        <a class="nav-link" href="{{ route('superadmin.meetings.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Meetings
                        </a>
                        <div class="sidenav-menu-heading">Reference</div>
                        <a class="nav-link" href="{{ route('superadmin.companies.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Companies
                        </a>
                        <a class="nav-link" href="{{ route('superadmin.projects.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Projects
                        </a>
                        <a class="nav-link" href="{{ route('superadmin.users.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Users
                        </a>
                        @endif
                        @if($isSupport)
                        <a class="nav-link" href="{{ route('superadmin.tickets.index') }}">
                            <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                            Tickets
                        </a>
                        @endif
                        @if($isClient)
                        @endif
                    </div>
                </div>
                <div class="sidenav-footer">
                    <div class="sidenav-footer-content">
                        <div class="sidenav-footer-subtitle">Logged in as:</div>
                        <div class="sidenav-footer-title">{{ $user->email }}</div>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">    
            @yield('header')
            @yield('content')
            <footer class="footer mt-auto footer-light">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 small">Copyright &copy; #</div>
                        <div class="col-md-6 text-md-right small">
                            <a href="#!">Privacy Policy</a>
                            &middot;
                            <a href="#!">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.27.0/feather.min.js" crossorigin="anonymous"> </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"> </script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{ $baseAsset }}/js/scripts.js"></script>
    @yield('scripts')
</body>

</html>