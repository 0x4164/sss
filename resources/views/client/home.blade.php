@extends('layouts.base-admin')
@php
$baseAsset = asset('assets');
$asset = asset('');
$user = auth()->user();
@endphp
@section('content')
<main>
    <header class="page-header page-header-dark bg-primary pb-10">
        <div class="text-center bg-dark text-white py-2">Lihat profil dan <i class="fa fa-qrcode"></i> QR Code Anda <a href="#" class="text-yellow">di sini</a></div>
        <div class="container">

            <div class="page-header-content pt-4 mb-3">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-9 mt-4">
                        <h1 class="page-header-title">
                            Dashboard
                        </h1>
                        <div class="page-header-subtitle">Executive Information System</div>

                    </div>

                </div>

            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container mt-n15">
    <div class="row mb-5">
            <div class="col-lg-4">
                <div class="card bg-primary border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data kehadiran Anda</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">80%</span>
                            <span class="text-white-50">(8/10 rapat)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-75 rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card bg-warning border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data Aspirasi</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">100%</span>
                            <span class="text-white-50">(2/2 sudah direspon)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-100 rounded-pill" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card bg-primary border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data Reses</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">80%</span>
                            <span class="text-white-50">(8/10 rapat)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-75 rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>
@endsection
@section('scripts')
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title no_laporan">Detail data</h5>
                <button class="btn btn-danger btn-sm cetak-detail ml-3"><i class="fas fa-print ml-1"></i>
                    Cetak</button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script>
    console.log("announce begin");
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    
    // instances
    
    
</script>
@endsection
