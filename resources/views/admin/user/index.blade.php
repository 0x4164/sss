@extends('layouts.base-admin')
<?php
$asset = asset('');
?>
@section('content')
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-2">
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="mx-3 mt-n10">
        <div class="card mb-4">
            <div class="card-header">
                <a href="#" class="btn btn-success add float-right btn-sm ">Buat user</a>
            </div>
            <div class="card-body">
                <h1>
                    Users
                </h1>
                <hr>
                <table id="datatable" class="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>role</th>
                            <th>username</th>
                            <th>email</th>
                            <th>created</th>
                            <th>active</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@component('partials.modal')
    @section('modal-content')
        <form id="form" data-id="" data-satker="" class="form-horizontal" method="post">
            <div class="modal-body card-body">
                <div class="">
                    <div class="form-group input-deskripsi">
                        <div class="col-lg-12">
                            <h3>
                                Basic
                            </h3>
                            Info: default password = 12345678 / password
                            @csrf
                            <!-- {{ method_field('POST') }} -->
                            {!! App\Helpers\AppForm::input('text', "Name", "name", true) !!}
                            {!! App\Helpers\AppForm::input('text', "Username", "username", true) !!}
                            {!! App\Helpers\AppForm::input('email', "Email", "email", true) !!}
                            <input type="hidden" name="email_verified_at" value="{{ now() }}">
                            {!! App\Helpers\AppForm::select("role", "role", $roles, true, "value", "-")  !!}
                            {!! App\Helpers\AppForm::select("Aktif", "is_active", [0, 1], true, "value", "-")  !!}
                            {!! App\Helpers\AppForm::input('password', "Password", "password", false) !!}
                            {!! App\Helpers\AppForm::input('password', "Re password", "repassword", false) !!}
                            <hr>
                            <h3>
                                Profile
                            </h3>
                            <!-- to do : 
                            - user profile
                            - assign to company
                            - assign to team
                            - assign to ticket -->
                        </div>
                    </div>
                </div>
                <!-- todo : use appform -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    @endsection
@endcomponent
@endsection

@section('scripts')
<script src="{{ $asset.'assets/js/appdatatable.js' }}"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script src="{{ $asset.'assets/js/app.js' }}"></script>
<script>
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    var func = function(arg){}
    console.log("Hello")

    var routes = {
        create:'{{ route("superadmin.users.store") }}',
        read:'{{ route("superadmin.users.show",["id"=>"idx"]) }}',
        update:'{{ route("superadmin.users.update",["id"=>"idx"]) }}',
        delete:'{{ route("superadmin.users.destroy",["id"=>"idx"]) }}',
        datatable: '{{ route("admin.users.datatable") }}'
    }

    $().DataTable()
    var dt = dtCRUDFactory("#datatable", routes.datatable, [
        AppDatatable.util.colNumbering('id','id'),
        AppDatatable.cols.basicFormat("role", "role"),
        AppDatatable.cols.basicFormat("username", "username"),
        AppDatatable.cols.basicFormat("email", "email"),
        AppDatatable.cols.basicFormat("created_at", "created_at"),
        AppDatatable.cols.basicFormat("is_active", "is_active"),
    ])

    var form = {
        main: {
            set(data){
                $("input[name=name]").val(data.name)
                $("input[name=username]").val(data.username)
                $("input[name=email]").val(data.email)
                $("select[name=role]").val(data.role)
                $("select[name=is_active]").val(data.is_active)
                $("input[name=password]").val(data.password)
                $("input[name=repassword]").val(data.repassword)
            },
            reset(){
                form.main.set({
                    name: "Nama",
                    username: "username",
                    email: "a@gmail.com",
                    role: "support",
                    password: "12345678",
                    repassword: "12345678",
                    is_active: 1,
                })
            }
        }
    }

    let crud = initCRUD()
</script>
@endsection