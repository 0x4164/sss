@extends('layouts.base-admin')
<?php
$asset = asset('');
$tid = $ticket->id;
?>
@section('content')
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-2">
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="mx-3 mt-n10">
        <div class="card mb-4">
            <div class="card-header">
                <a href="#" class="btn btn-success add float-right btn-sm ">Update status</a>
            </div>
            <div class="card-body">
                <h1>
                    Tiket Logs #
                </h1>
                <a href="{{ route('superadmin.tickets.index') }}" class="btn btn-success btn-sm">Kembali</a>
                <hr>
                <pre>
                    <?php preson($ticket) ?>
                </pre>
                <hr>
                <div class="table-responsive">
                    <table id="datatable" class="table">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>tid</th>
                                <th>Descr</th>
                                <th>Status</th>
                                <th>agent</th>
                                <th>created</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@component('partials.modal')
    @section('modal-content')
        <form id="form" data-id="" data-satker="" class="form-horizontal" method="post">
            <div class="modal-body card-body">
                <div class="">
                    <div class="form-group input-deskripsi">
                        <div class="col-lg-12">
                            <h3>
                                Basic
                            </h3>
                            <button id="btBug" type="button" class="btn btn-danger float-right btn-sm" onclick="act('reset-test')">Test</button>
                            @csrf
                            <!-- {{ method_field('POST') }} -->
                            <input type="hidden" name="tickets_id">
                            {!! App\Helpers\AppForm::input('text', "descr", "descr", true) !!}
                            {!! App\Helpers\AppForm::selectModel("status", "status", $ticketStats, "id", "name", true, "value", "-")  !!}
                            <hr>
                            <h3>
                                Agents
                            </h3>
                            Ditangani oleh:
                            {!! App\Helpers\AppForm::selectModel("Agents", "agent", $agents, "id", "username", true, "val") !!}
                            <!-- to do : 
                            - relations -->
                        </div>
                    </div>
                </div>
                <!-- todo : use appform -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    @endsection
@endcomponent
@endsection

@section('scripts')
<script src="{{ $asset.'assets/js/appdatatable.js' }}"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script src="{{ $asset.'assets/js/app.js' }}"></script>
<script>
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    var func = function(arg){}
    console.log("Hello")

    var routes = {
        create:'{{ route("superadmin.tickets-logs.createFromTicket", ["id"=>"idx"]) }}',
        read:'{{ route("superadmin.tickets-logs.show",["id"=>"idx"]) }}',
        update:'{{ route("superadmin.tickets-logs.update",["id"=>"idx"]) }}',
        delete:'{{ route("superadmin.tickets-logs.destroy",["id"=>"idx"]) }}',
        datatable: '{{ route("admin.tickets-logs.datatable") }}',
        dtFilter: '{{ route("admin.tickets-logs.datatable.filter") }}',
    }


    var dtParams = {
        tickets_id: {{ $ticket->id }}
    }
    $("input[name=tickets_id]").val(dtParams.tickets_id)

    var form = {
        main: {
            set(data){
                $("input[name=descr]").val(data.descr)
                $("input[name=tickets_id]").val(dtParams.tickets_id)
                $("input[name=status]").val(data.status)
                $("select[name=status]").val(data.status)
                $("select[name=agent]").val(data.agent)
            },
            reset(type = null){
                var d = {
                    agent:{

                }}

                switch(type){
                    case "test":
                        d = {
                            descr: "Saya punya masalah di test",
                            status: "pending",
                            agent: "admin",
                        }
                    break;
                    default:
                }
                form.main.set(d)
            }
        }
    }

    const act = (d) =>{
        switch(d){
            case "reset-test":
                form.main.reset('test')
            break;
        }
    }

    function setDatatable(t = "filter"){
        let url = routes.dtFilter
        switch(t){
            case "filter":
                const sp = new URLSearchParams({
                    "wheres[tickets_id]": dtParams.tickets_id
                })
                cl(sp.toString())
                url += "?" + sp.toString()
            break;
        }

        let dt = dtCRUDFactory("#datatable", url, [
            AppDatatable.util.colNumbering('id','id'),
            AppDatatable.cols.basicFormat("tickets_id", "tickets_id"),
            AppDatatable.cols.basicFormat("descr", "descr"),
            AppDatatable.cols.basicFormat("status", "status"),
            AppDatatable.cols.basicRender("agent", function(data){
                // to do : link
                return data.username
            }),
            AppDatatable.cols.basicFormat("created_at", "created_at"),
        ])

        return dt
    }

    class CRUDExt extends CRUD{
        setCreate(id){
            id = dtParams.tickets_id
            let ctx = this
            
            ajaxer.post(this.routes.create.replace("idx", id), 
                new FormData(
                    document.getElementById('form')
                ), 
                function(response){
                    if(response.success){
                        alert('Data disimpan');
                        // window.location.reload()
                        ctx.reloadDatatable()
                        $('#modal').modal("hide");
                    }else{
                        alert(response.message)
                    }
                },
            )
        }
    }
    // override default route
    let crud = new CRUDExt()
    crud.routes = routes
    crud.routes.read = routes.complete
    var dt = setDatatable("filter")
    crud.setDatatable(dt)
    let c = generalCRUD(crud)
</script>
@endsection