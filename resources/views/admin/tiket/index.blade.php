@extends('layouts.base-admin')
<?php
$asset = asset('');
?>
@section('content')
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-2">
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="mx-3 mt-n10">
        <div class="card mb-4">
            <div class="card-header">Tickets
                <a href="#" class="btn btn-success add-program float-right btn-sm ">Create ticket</a>
                <a href="{{ route('superadmin.ticket.index-tbl') }}" class="btn btn-success add-program float-right btn-sm ">Table view</a>
            </div>
            <div class="card-body">
                <form class="form-inline">
                    <div class="form-group mb-2">
                        <label for="staticEmail2" class="">Filter by project</label>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">Password</label>
                        <input type="text" class="form-control form-control-sm">
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary mb-2">Submit</button>
                </form>
                <hr>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header bg-success text-white">Open</div>
                            <div class="card-body">
                                <h5 class="card-title text-orange mb-0">
                                    <small>Integration</small> <br>
                                    <span class="badge badge-primary badge-sm">E-Tilang</span> <span class="badge badge-success badge-sm">General Issue</span>
                                </h5>
                                
                                <small>Date create: 01-01-2021 | Jefry</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header bg-teal text-white">In Progress</div>
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header bg-warning text-white">On Hold</div>
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <div class="card-header bg-primary text-white">Closed</div>
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@component('partials.modal')
    @section('modal-content')
        <form action="" id="formTambah" data-id='' data-satker='' class="form-horizontal" method="post">
            <div class="modal-body card-body">
                <div class="">
                    <div class="form-group input-deskripsi">
                        <label class="control-label col-lg-12">by</label>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" name="tahun">
                        </div>
                    </div>
                </div>
                <!-- todo : use appform -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    @endsection
@endcomponent
@endsection

@section('scripts')
<script src="{{ $asset.'assets/js/appdatatable.js' }}"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script>
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    var func = function(arg){}
    console.log("Hello")

    $(document).on('click', ".add-program", function() {
        $('#modal_form').modal("show");
    });

    var lyRoutes = {
        create:'{{ route("superadmin.tickets.store") }}',
        read:'{{ route("superadmin.tickets.show",["id"=>"idx"]) }}',
        update:'{{ route("superadmin.tickets.update",["id"=>"idx"]) }}',
        delete:'{{ route("superadmin.tickets.destroy",["id"=>"idx"]) }}',
        datatable: 'todo'
    }

    var dt = dtCRUDFactory("#datatable", lyRoutes.datatable, [
        AppDatatable.util.colNumbering('id','id'),
        AppDatatable.cols.basicFormat("nama", "nama"),
        AppDatatable.cols.basicFormat("deskripsi", "deskripsi"),
        AppDatatable.cols.basicFormat("kode", "kode"),
    ])

    var form = {
        main: {
            set(data){
                $("input[name=nama]").val(data.nama)
                $("input[name=kode]").val(data.kode)
                $("textarea[name=deskripsi]").val(data.deskripsi)
            }
        }
    }

    $(document).on('submit', '#form_add', function(e) {
        e.preventDefault();
        var data = new FormData(this);
        var id = $(this).attr("data-id");

        if(id){
            // alert("update")
            var url = lyRoutes.update.replace("idx", id)
            ajaxer.post(url, new FormData(this), 
                function(response){
                    alert('Data updated');
                    // window.location.reload()
                    $('#modal_add').modal("hide");
                    dt.ajax.reload()
                },
            )
        }else{
            // alert("create")
            ajaxer.post(lyRoutes.create, new FormData(this), 
                function(response){
                    if(response.success){
                        alert('Data disimpan');
                        // window.location.reload()
                        dt.ajax.reload()
                        $('#modal_add').modal("hide");
                    }else{
                        alert(response.message)
                    }
                },
            )
        }
    });

    $(document).on('click', '#btDelete', function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");

        ajaxer.delete(lyRoutes.delete.replace("idx", id), null, 
            function(response){
                alert('Data terhapus');
                // window.location.reload()
                dt.ajax.reload()
            },
        )
    });

    $(document).on('click', '#btEdit', function(e) {
        e.preventDefault();
        var id = $(this).attr("data-id");

        ajaxer.get(lyRoutes.read.replace("idx", id), null, 
            function(response){
                $('#form_add').attr("data-id", id);
                console.log(response)
                form.main.set(response.data)
            },
        )
    });
</script>
@endsection