@extends('layouts.base-admin')
<?php
$asset = asset('');

$user = auth()->user();

$isSuperadmin = $user->isSuperadmin();

?>
@section('content')
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-2">
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="mx-3 mt-n10">
        <div class="card mb-4">
            <div class="card-body">
                <h1>
                    Tiket
                    <a href="#" class="btn btn-success add float-right btn-sm ">Buat</a>
                </h1>
                <hr>
                <form class="form-inline">
                    <div class="form-group mx-sm-3 mb-2">
                        <select class="form-control" name="f-status" id="f-status">
                            <option value="-">-Status-</option>
                        </select>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <select class="form-control" name="f-project" id="f-project">
                            <option value="-">-Project-</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-success mb-2" onclick="setDatatable('filter')">Filter</button>
                </form>
                <hr>
                <div class="table-responsive">
                    <table id="datatable" class="table">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>nama</th>
                                <th>project</th>
                                <th>by</th>
                                <th>last_status</th>
                                <th>agent</th>
                                <th>created</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@component('partials.modal')
    @section('modal-content')
        <form id="form" data-id="" data-satker="" class="form-horizontal" method="post">
            <div class="modal-body card-body">
                <div class="">
                    <div class="form-group input-deskripsi">
                        <div class="col-lg-12">
                            <h3>
                                Basic
                            </h3>
                            <button id="btBug" type="button" class="btn btn-danger float-right btn-sm" onclick="act('reset-bug')">Bug</button>
                            @csrf
                            <!-- {{ method_field('POST') }} -->
                            {!! App\Helpers\AppForm::input('text', "name", "name", true) !!}
                            {!! App\Helpers\AppForm::textarea("Deskripsi", "descr", "") !!}
                            <button type="button" class="btn btn-warning btn-sm float" onclick="act('resize-tx-100')">100</button>
                            <button type="button" class="btn btn-warning btn-sm float" onclick="act('resize-tx-300')">300</button>
                            <!-- input('text', "descr", "descr", true) -->
                            <input type="hidden" name="byv">
                            {!! App\Helpers\AppForm::selectModel("category", "category", $tags, "id", "name", true, "val") !!}
                            {!! App\Helpers\AppForm::selectModel("company", "company_id", $comp, "id", "name", true, "val") !!}
                            {!! App\Helpers\AppForm::selectModel("project", "project_id", $proj, "id", "name", true, "val") !!}
                            <a id="btToLog" href="" target="_blank" class="btn btn-success btn-sm">Log</a>
                            <!-- to do : 
                            - relations -->
                        </div>
                    </div>
                </div>
                <!-- todo : use appform -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    @endsection
@endcomponent
@endsection

@section('scripts')
<script src="{{ $asset.'assets/js/appdatatable.js' }}"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script src="{{ $asset.'assets/js/app.js' }}"></script>
<script>
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    var func = function(arg){}
    console.log("Hello")

    // @if($isSuperadmin)

    var routes = {
        create:'{{ route("superadmin.tickets.store") }}',
        read:'{{ route("superadmin.tickets.show",["id"=>"idx"]) }}',
        complete:'{{ route("superadmin.tickets.complete",["id"=>"idx"]) }}',
        update:'{{ route("superadmin.tickets.update",["id"=>"idx"]) }}',
        delete:'{{ route("superadmin.tickets.destroy",["id"=>"idx"]) }}',
        logByTicket:'{{ route("superadmin.tickets-logs.byticket",["id"=>"idx"]) }}',
        getProjects:'{{ route("superadmin.projects.bycompany",["id"=>"idx"]) }}',
        datatable: '{{ route("admin.tickets.datatable") }}',
        dtFilter: '{{ route("admin.tickets.datatable.filter") }}',
        dtWithAgent: '{{ route("admin.tickets.datatable.withAgent") }}',
    }
    
    // @else

    var routes = {
        create:'{{ route("client.tickets.store") }}',
        read:'{{ route("client.tickets.show",["id"=>"idx"]) }}',
        complete:'{{ route("client.tickets.complete",["id"=>"idx"]) }}',
        update:'{{ route("client.tickets.update",["id"=>"idx"]) }}',
        delete:'{{ route("client.tickets.destroy",["id"=>"idx"]) }}',
        datatable: '{{ route("admin.client.tickets.datatable") }}',
        dtFilter: '{{ route("admin.client.tickets.datatable.filter") }}',
        logByTicket:'{{ route("superadmin.tickets-logs.byticket",["id"=>"idx"]) }}',
    }
    
    // @endif

    var form = {
        main: {
            set(data){
                $("#btToLog").attr("href", routes.logByTicket.replace("idx", data.id))
                $("input[name=name]").val(data.name)
                $("input[name=descr]").val(data.descr)
                $("textarea[name=descr]").val(data.descr)
                if(data.descr){
                    if(data.descr.length >= 200){
                        act('resize-tx-300')
                    }else{
                        act('resize-tx-100')
                    }
                }
                // $("input[name=byv]").val(data.by)
                $("input[name=by]").val(data.by)
                // if( data.by != "reset" ){
                //     $("input[name=byv]").attr("disabled", "disabled");
                // }else{
                //     $("input[name=byv]").removeAttr("disabled");
                // }
                $("select[name=category]").val(data.category)
                $("select[name=last_status]").val(data.last_status)
                $("select[name=agent]").val(data.agent.id)
                $("select[name=company_id]").val(data.company.id)
                $("select[name=project_id]").val(data.project.id)
            },
            reset(type = null){
                var d = {
                    agent:{},
                    company:{},
                    project:{},
                }

                switch(type){
                    case "bug":
                        d = {
                            name: "Bug",
                            descr: "Saya punya masalah di bug",
                            last_status: "pending",
                            by: "admin",
                            agent:{},
                            company:{},
                            project:{},
                        }
                    break;
                    default:
                }
                form.main.set(d)
            }
        }
    }

    const tiketStats = <?= $ticketStats ? json_encode($ticketStats) : "[]" ?>
    
    const projects = <?= $proj ? json_encode($proj) : "[]" ?>

    const act = function(d, data={}){
        switch(d){
            case "reset-bug":
                form.main.reset('bug')
            break;
            case "resize-tx":
                $("textarea[name=descr]")[0].style.height = data.height
            break;
            case "resize-tx-100":
                act("resize-tx", {height: "100px"})
            break;
            case "resize-tx-300":
                act("resize-tx", {height: "300px"})
            break;
        }
    }

    async function getProjectByCompany(id){
        return await ajaxer.get(routes.getProjects.replace("idx", id))
    }

    $("select[name=company_id]").on('change', async function(){
        const d = await getProjectByCompany(
            $(this).val()
        )
        if(d.success){
            let opts = renderOpt(d.data, {
                value: "id",
                label: "name",
            })
            $("select[name=project_id]").html("")
            $("select[name=project_id]").html(opts)
        }
    })

    function setupFilters(){
        let opts = renderOpt(projects, {
            value: "id",
            label: "name",
        }, "-Projects-")
        $("select[name=f-project]").html("")
            .html(opts)
        
        opts = renderOpt(tiketStats, "arr", "-Status-")
        $("select[name=f-status]").html("")
            .html(opts)
    }

    function getSpan(t = ""){
        let ret = ""
        switch(t){
            case "success":
                ret = `<span class="badge badge-success">[text]</span>`;
            break;
            case "danger":
                ret = `<span class="badge badge-danger">[text]</span>`;
            break;
            case "primary":
                ret = `<span class="badge badge-primary">[text]</span>`;
            break;
            case "warning":
                ret = `<span class="badge badge-warning">[text]</span>`;
            break;
            case "solved":
                ret = getSpan("success").replace("[text]", "Solved")
            break;
            case "open":
                ret = getSpan("danger").replace("[text]", "Open")
            break;
            case "closed":
                ret = getSpan("primary").replace("[text]", "Closed")
            break;
            case "in-progress":
                ret = getSpan("warning").replace("[text]", "In Progress")
            break;
            case "pending":
                ret = getSpan("warning").replace("[text]", "Pending")
            break;
            default:
                ret = getSpan("warning").replace("[text]", "-")
        }
        return ret
    }

    class DTCRUDExt extends DTCRUD{
        getDefaultNav(d){
            var ret = [
                `<button id="btEdit" class="btn btn-sm btn-primary btn-edit" data-toggle="modal" data-target="#modal" data-id="${d.id}"> Edit </button>`,
                `<button id="btDelete" class="btn btn-sm btn-danger btn-delete" data-id="${d.id}"> Delete</button>`,
                `<a href="${routes.logByTicket.replace("idx", d.id)}" 
                target="_blank" class="bt-log btn btn-success btn-sm" data-id="${d.id}">Log</a>`,
            ]
            return ret;
        }
    }

    const dtCrudExt = new DTCRUDExt()

    function setDatatable(t = "filter"){
        let url = routes.dtFilter
        switch(t){
            case "filter":
                const sp = new URLSearchParams({
                    "wheres[last_status]": $('select[name=f-status]').val(),
                    "wheres[project_id]": $('select[name=f-project]').val(),
                })
                // cl(sp.toString())
                url += "?" + sp.toString()
            break;
        }

        let dt = dtCrudExt.build("#datatable", url, [
            AppDatatable.util.colNumbering('id','id'),
            AppDatatable.cols.basicFormat("name", "name"),
            AppDatatable.cols.basicRender("id", function(data, type, row){
                // to do : link
                return row.project.name
            }),
            AppDatatable.cols.basicRender("by_user", function(data){
                // to do : link
                var ret = ""
                if(data){
                    ret = data.username
                }
                
                return ret
            }),
            AppDatatable.cols.basicRender("last_status", function(data){
                // to do : link
                return getSpan(data)
            }),
            AppDatatable.cols.basicRender("get_agent", function(data){
                // to do : link
                return data.username
            }),
            AppDatatable.cols.basicRender("created_at", function(data){
                let d = new Date(data);
                // cl(d)
                return util.dmyhm(d)
            }),
        ])

        return dt
    }

    // override default route
    let crud = new CRUD()
    crud.routes = routes
    crud.routes.read = routes.complete
    var dt = setDatatable()
    crud.setDatatable(dt)
    let c = generalCRUD(crud)
    setupFilters()
</script>
@endsection