@extends('layouts.base-admin')
@php
$baseAsset = asset('assets');
$asset = asset('');
$user = auth()->user();
@endphp
@section('content')
<main>
    <header class="page-header page-header-dark bg-primary pb-10">
        <div class="text-center bg-dark text-white py-2">Lihat profil dan <i class="fa fa-qrcode"></i> QR Code Anda <a href="#" class="text-yellow">di sini</a></div>
        <div class="container">

            <div class="page-header-content pt-4 mb-3">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-9 mt-4">
                        <h1 class="page-header-title">
                            Dashboard
                        </h1>
                        <div class="page-header-subtitle">Executive Information System</div>

                    </div>

                </div>

            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container mt-n15">
        <!-- Area chart example-->
        <div class="row mb-5">
            <div class="col-lg-4">
                <div class="card bg-primary border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data kehadiran Anda</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">80%</span>
                            <span class="text-white-50">(8/10 rapat)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-75 rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card bg-warning border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data Aspirasi</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">100%</span>
                            <span class="text-white-50">(2/2 sudah direspon)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-100 rounded-pill" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card bg-primary border-0">
                    <div class="card-body">
                        <h5 class="text-white-50">Data Reses</h5>
                        <div class="mb-4">
                            <span class="display-4 text-white">80%</span>
                            <span class="text-white-50">(8/10 rapat)</span>
                        </div>
                        <div class="progress bg-white-25 rounded-pill" style="height: 0.5rem;">
                            <div class="progress-bar bg-white w-75 rounded-pill" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-12">
            <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Jadwal Rapat Anda
                        <div class="dropdown no-caret">
                            <button class="btn btn-transparent-dark btn-icon dropdown-toggle" id="dropdownMenuButton" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical text-gray-500"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg></button>
                            <div class="dropdown-menu dropdown-menu-right animated--fade-in-up" aria-labelledby="dropdownMenuButton">
                                <h6 class="dropdown-header">Filter Activity:</h6>
                                <a class="dropdown-item" href="#!"><span class="badge badge-green-soft text-green my-1">Commerce</span></a>
                                <a class="dropdown-item" href="#!"><span class="badge badge-blue-soft text-blue my-1">Reporting</span></a>
                                <a class="dropdown-item" href="#!"><span class="badge badge-yellow-soft text-yellow my-1">Server</span></a>
                                <a class="dropdown-item" href="#!"><span class="badge badge-purple-soft text-purple my-1">Users</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="timeline timeline-xs">
                            <!-- Timeline Item 1-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">27 min</div>
                                    <div class="timeline-item-marker-indicator bg-green"></div>
                                </div>
                                <div class="timeline-item-content">
                                    New order placed!
                                    <a class="font-weight-bold text-dark" href="#!">Order #2912</a>
                                    has been successfully placed.
                                </div>
                            </div>
                            <!-- Timeline Item 2-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">58 min</div>
                                    <div class="timeline-item-marker-indicator bg-blue"></div>
                                </div>
                                <div class="timeline-item-content">
                                    Your
                                    <a class="font-weight-bold text-dark" href="#!">weekly report</a>
                                    has been generated and is ready to view.
                                </div>
                            </div>
                            <!-- Timeline Item 3-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">2 hrs</div>
                                    <div class="timeline-item-marker-indicator bg-purple"></div>
                                </div>
                                <div class="timeline-item-content">
                                    New user
                                    <a class="font-weight-bold text-dark" href="#!">Valerie Luna</a>
                                    has registered
                                </div>
                            </div>
                            <!-- Timeline Item 4-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">1 day</div>
                                    <div class="timeline-item-marker-indicator bg-yellow"></div>
                                </div>
                                <div class="timeline-item-content">Server activity monitor alert</div>
                            </div>
                            <!-- Timeline Item 5-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">1 day</div>
                                    <div class="timeline-item-marker-indicator bg-green"></div>
                                </div>
                                <div class="timeline-item-content">
                                    New order placed!
                                    <a class="font-weight-bold text-dark" href="#!">Order #2911</a>
                                    has been successfully placed.
                                </div>
                            </div>
                            <!-- Timeline Item 6-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">1 day</div>
                                    <div class="timeline-item-marker-indicator bg-purple"></div>
                                </div>
                                <div class="timeline-item-content">
                                    Details for
                                    <a class="font-weight-bold text-dark" href="#!">Marketing and Planning Meeting</a>
                                    have been updated.
                                </div>
                            </div>
                            <!-- Timeline Item 7-->
                            <div class="timeline-item">
                                <div class="timeline-item-marker">
                                    <div class="timeline-item-marker-text">2 days</div>
                                    <div class="timeline-item-marker-indicator bg-green"></div>
                                </div>
                                <div class="timeline-item-content">
                                    New order placed!
                                    <a class="font-weight-bold text-dark" href="#!">Order #2910</a>
                                    has been successfully placed.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@section('scripts')
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title no_laporan">Detail data</h5>
                <button class="btn btn-danger btn-sm cetak-detail ml-3"><i class="fas fa-print ml-1"></i>
                    Cetak</button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<script src="{{ $baseAsset }}/js/announcer.js"></script>
<script src="{{ $baseAsset }}/js/fetcher.js"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script>
    console.log("announce begin");
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    
    // instances
    
    
</script>
@endsection
