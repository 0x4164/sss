@extends('layouts.base-admin')
<?php
$asset = asset('');
?>
@section('content')
<main>
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-2">
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="mx-3 mt-n10">
        <div class="card mb-4">
            <div class="card-header">
                <a href="#" class="btn btn-success add float-right btn-sm ">Buat</a>
            </div>
            <div class="card-body">
                <h1>
                    Company
                </h1>
                <hr>
                <table id="datatable" class="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>descr</th>
                            <th>address</th>
                            <th>main_contact</th>
                            <th>created</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
@component('partials.modal')
    @section('modal-content')
        <form id="form" data-id="" data-satker="" class="form-horizontal" method="post">
            <div class="modal-body card-body">
                <div class="">
                    <div class="form-group input-deskripsi">
                        <div class="col-lg-12">
                            <h3>
                                Basic
                            </h3>
                            @csrf
                            <!-- {{ method_field('POST') }} -->
                            {!! App\Helpers\AppForm::input('text', "name", "name", true) !!}
                            {!! App\Helpers\AppForm::input('text', "descr", "descr", true) !!}
                            {!! App\Helpers\AppForm::input('text', "address", "address", true) !!}
                            {!! App\Helpers\AppForm::input('text', "Main contact", "main_contact", true) !!}
                            <!-- lembaga -->
                            {!! App\Helpers\AppForm::selectModel("lembaga", "lembaga", $lembaga, "id", "name", true, "val") !!}

                            
                            <!-- to do : 
                            - relation data -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    @endsection
@endcomponent
@endsection

@section('scripts')
<script src="{{ $asset.'assets/js/appdatatable.js' }}"></script>
<script src="{{ $asset.'assets/js/ajaxer.js' }}"></script>
<script src="{{ $asset.'assets/js/app.js' }}"></script>
<script>
    var ajaxer = new Ajaxer()
    ajaxer.withCsrf()
    var func = function(arg){}
    console.log("Hello")

    var routes = {
        create:'{{ route("superadmin.companies.store") }}',
        read:'{{ route("superadmin.companies.show",["id"=>"idx"]) }}',
        update:'{{ route("superadmin.companies.update",["id"=>"idx"]) }}',
        delete:'{{ route("superadmin.companies.destroy",["id"=>"idx"]) }}',
        datatable: '{{ route("admin.companies.datatable") }}'
    }

    var dt = dtCRUDFactory("#datatable", routes.datatable, [
        AppDatatable.cols.basicFormat("id", "id"),
        AppDatatable.cols.basicFormat("name", "name"),
        AppDatatable.cols.basicFormat("descr", "descr"),
        AppDatatable.cols.basicFormat("address", "address"),
        AppDatatable.cols.basicFormat("main_contact", "main_contact"),
        AppDatatable.cols.basicFormat("created_at", "created_at"),
    ])

    var form = {
        main: {
            set(data){
                $("input[name=id]").val(data.id)
                $("input[name=name]").val(data.name)
                $("input[name=descr]").val(data.descr)
                $("input[name=address]").val(data.address)
                $("input[name=main_contact]").val(data.main_contact)
                $("select[name=lembaga]").val(data.lembaga)
            },
            reset(){
                form.main.set({})
            }
        }
    }

    initCRUD()
</script>
@endsection