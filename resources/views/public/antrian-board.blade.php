@extends('layouts.base-admin')
@php
$baseAsset = asset('assets');
@endphp
@section('content')
<section class="bg-white py-5">
    <div class="mx-3">
        <div class="row pb-10">
            @if(sizeof($withAntrian) > 0)
                @foreach($withAntrian as $row)
                    <?php
                        $eid = "layanan".$row->id;
                    ?>
                    <div class="col-lg-3 col-md-6 mb-5">
                        <a class="card card-link h-100 lift mb-2" href="#!">
                            <div class="card-header text-dark">{{ $row->nama }}</div>
                            <div class="card-body" id="{{ $eid."printable" }}">
                                <h2 class="font-weight-bold text-center">Umum</h2>
                                <div class="text-center">Nomor antrian Anda</div>
                                <h1 id="{{ $eid."max" }}" class="text-center" style="font-size: 80px;">
                                    {{ $row->max_antrian ? $row->max_antrian : '-' }}
                                </h1>
                                <ul>
                                    <li>{{ $row->deskripsi }}</li>
                                </ul>
                            </div>
                            <div class="card-footer">
                            <button class="btn btn-danger btn-lg btn-block btn-antrian"
                                data-id="{{ $row->id }}"
                                onclick="act('{{ $eid }}', 'ambil-cetak')"
                                >Ambil Antrian</button>
                            </div>
                        </a> 
                    </div> 
                @endforeach
            @else
                <div class="col-lg-3 col-md-6 mb-5">
                    <a class="card card-link h-100 lift mb-2" href="#!">
                        <div class="card-header text-dark">-</div>
                        <div class="card-body" id="">
                            <h2 class="font-weight-bold text-center">-</h2>
                            Antrian belum dibuka
                        </div>
                        <div class="card-footer">
                        <button class="btn btn-lg btn-block btn-antrian" disabled>Ambil Antrian</button>
                        </div>
                    </a> 
                </div> 
            @endif
            
        </div>
    </div>
</section>
@endsection
@section('scripts')
<?php
if($withAntrian && isset($row)){
?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title no_laporan">Detail</h5>
                <button class="btn btn-danger btn-sm cetak-detail ml-3"
                    data-id="{{ $row->id }}"
                    onclick="act('{{ $eid }}', 'ambil-cetak')"
                    >
                    <i class="fas fa-print ml-1"></i>
                    Cetak
                </button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php
}else{
    echo "kosong";
}
?>
<script type="text/javascript" src="{{ $baseAsset }}js/moment/moment-with-locales.js"></script>
<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
<script src="{{ $baseAsset }}/js/announcer.js"></script>
<script src="{{ $baseAsset }}/js/fetcher.js"></script>
<script>
    var layananDb = <?= $withAntrian ? json_encode($withAntrian) : 'null'?>;
</script>
<script>
    console.log("board begin");

    var queues = {}
    var fetcher = new Fetcher()
    var urls = {
        antrian:{
            create: '{{ route("public.antrian.push") }}'
        }
    }

    layananDb.forEach(function(e){
        var idx = "layanan"+e.id
        queues[idx] = (new Queue(
            e.id,
            e.nama_layanan,
            e.deskripsi,
            null
        ))
        .setRole("public-board")
        .setCurrent(e.id, e.cnt_uncalled, e.cnt_called, e.max_antrian - 1)
        .setCallbacks({
            take:(queue) =>{
                console.log("take")
                console.log("take : "+queue.current.num)
            },
            next:(queue) =>{
                console.log("next take")
                fetcher.post(urls.antrian.create, {
                    id_layanan: e.id
                }).then((data)=>{
                    if(data.success){
                        $("#"+idx+"max").text(data.data.no_antrian)
                    }else{
                        queue.undoNext()
                    }
                })
                // setTimeout(()=>{
                // },1000)
            },
            print:(queue) =>{
                console.log("print")
                console.log("print : "+queue.current.num)

                queue.next()
                if(queue.current.num > -1){
                    printJS(idx+"printable", 'html')
                }
            },
        })
        console.log(e.nama_layanan)
    })

    function act(eid = null, type = null){
        var Ly = queues[eid]
        switch(type){
            case "ambil":
                Ly.take()
            break;
            case "print":
                Ly.print()
            break;
            case "ambil-cetak":
                Ly.take()
                Ly.print()
            break;
        }
    }
</script>
@endsection
