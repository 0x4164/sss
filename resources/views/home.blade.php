@extends('layouts.base')
@php
$baseAsset = asset('assets');
@endphp
@section('header')
<div class="page-header-content">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <h1 class="font-weight-bold page-header-title">Syssolusindo Support</h1>
                <p class="page-header-text mb-5">Jadikan layanan <b>Pelayanan Terpadu Satu Pintu</b></p>

                #fast #respon
            </div>
            <div class="col-lg-6 d-none d-lg-block"><img class="img-fluid"
                    src="{{ $baseAsset }}/images/banner.png"></div>
        </div>
    </div>
</div>
<div class="svg-border-rounded text-light">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none"
        fill="currentColor">
        <path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
    </svg>
</div>
@endsection

@section('content')
<section class="bg-light py-10">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-layers">
                        <polygon points="12 2 2 7 12 12 22 7 12 2"></polygon>
                        <polyline points="2 17 12 22 22 17"></polyline>
                        <polyline points="2 12 12 17 22 12"></polyline>
                    </svg></div>
                <h3>Online Form</h3>
                <p class="mb-0">Mudahkan publik mendapat informasi</p>
            </div>
            <div class="col-lg-4 mb-5 mb-lg-0">
                <div class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-smartphone">
                        <rect x="5" y="2" width="14" height="20" rx="2" ry="2"></rect>
                        <line x1="12" y1="18" x2="12.01" y2="18"></line>
                    </svg></div>
                <h3>User Friendly</h3>
                <p class="mb-0">Tampilan lebih modern dan dapat diakses melalui media apapun</p>
            </div>
            <div class="col-lg-4">
                <div class="icon-stack icon-stack-xl bg-gradient-primary-to-secondary text-white mb-4">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-code">
                        <polyline points="16 18 22 12 16 6"></polyline>
                        <polyline points="8 6 2 12 8 18"></polyline>
                    </svg></div>
                <h3>Keamanan</h3>
                <p class="mb-0">Kerahasiaan data terjamin.</p>
            </div>
        </div>
    </div>
</section>
<section class="bg-light pb-10">
    <div class="container">
        <div class="text-center mb-5">
            <h2>Rencanakan kebutuhan aplikasi Anda</h2>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Login admin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" class="form-signin" action="{{ route('login') }}">
                @csrf
                <div class="modal-body">

                    {{ '-' }}
                    <!-- Form Group (email address)-->
                    <div class="form-group">
                        <label class="small mb-1" for="inputEmailAddress">Username/ Email</label>
                        <input type="text" name="username" class="form-control" placeholder="Username">
                    </div>
                    <!-- Form Group (password)-->
                    <div class="form-group">
                        <label class="small mb-1" for="inputPassword">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Login <i
                            class="icon-circle-right2 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

